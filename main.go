package main

import (
	"fmt"
	"log"
	"os"

	"gopl.io/ch5/links"
)

//!+crawl
func crawl(url string) []string {
	fmt.Println(url)
	list, err := links.Extract(url)
	if err != nil {
		log.Print(err)
	}
	return list
}

//!-crawl

//!+main
// Processing goroutine includes receiver and sender
// goroutines max20:  RECEIVE unseenLinks (primary) -> SEND worklist
// main:              RECEIVE worklist (secondary) -> SEND unseenLinks
func main() {
	worklist := make(chan []string) // lists of URLs, may have duplicates
	unseenLinks := make(chan string)

	go func() { worklist <- os.Args[1:] }()

	// Create 20 crawler goroutines to fetch each unseen link
	for i := 0; i < 20; i++ {
		go func() {
			// RECEIVE unseenLinks
			for link := range unseenLinks {
				found := crawl(link)
				// SEND worklist
				go func() { worklist <- found }()
			}
		}()
	}

	// The main goroutine makes worklist items distinct
	// and sends unseen to the crawlers
	seen := make(map[string]bool)
	// RECEIVE worklist
	for list := range worklist {
		for _, link := range list {
			if !seen[link] {
				seen[link] = true
				// SEND unseenLinks
				unseenLinks <- link
			}
		}
	}
}

//!-main
